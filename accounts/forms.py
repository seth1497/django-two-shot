# from django.forms import ModelForm
from django import forms
# from accounts.models import Login

# class LoginForm(ModelForm):
#     class Meta:
#         model = Login
#         fields = "__all__"

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)

class SignupForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_confirmation = forms.CharField(max_length=150, widget=forms.PasswordInput)

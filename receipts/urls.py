from django.urls import path
from receipts.views import list_receipts, receipt_create

urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", receipt_create, name="create_receipt")
]

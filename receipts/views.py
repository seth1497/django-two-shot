



from django.shortcuts import render, redirect
from .models import Account, Receipt, ExpenseCategory
from .forms import ReceiptForm
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def list_receipts(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    # receipt_list = Receipt.objects.all()
    context = {"receipt_list": receipt_list}
    return render(request, "receipts/list.html", context)

# def create_receipt(request):


@login_required
def receipt_create(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
      # To redirect to the detail view of the model, use this:
            receipt_instance = form.save(False)
            receipt_instance.purchaser = request.user
            receipt_instance.save()
            return redirect("home")
      # To add something to the model, like setting a user,
      # use something like this:
      #
      # model_instance = form.save(commit=False)
      # model_instance.user = request.user
      # model_instance.save()
      # return redirect("detail_url", id=model_instance.id)
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create.html", context)

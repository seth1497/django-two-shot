from django.forms import ModelForm
from receipts.models import Account, Receipt, ExpenseCategory


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = "__all__"

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "category", "account", "date"]

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = "__all__"
